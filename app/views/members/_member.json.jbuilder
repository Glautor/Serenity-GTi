json.extract! member, :id, :name, :position, :balance, :created_at, :updated_at
json.url member_url(member, format: :json)
