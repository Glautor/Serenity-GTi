json.extract! project, :id, :name, :deadline, :client, :price, :status, :created_at, :updated_at
json.url project_url(project, format: :json)
