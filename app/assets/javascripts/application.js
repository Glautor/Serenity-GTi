// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require js/jquery-1.10.2.js
//= require js/jquery-ui-1.9.1.js
//= require bs3/js/bootstrap.min.js
//= require js/smooth-sliding-menu.js
//= require js/console-numbering.js
//= require js/to-do-admin.js
//= require js/jquery.sparkline.js
//= require js/select-checkbox.js
//= require js/jPushMenu.js
//= require plugins/scroll/jquery.nanoscroller.js
