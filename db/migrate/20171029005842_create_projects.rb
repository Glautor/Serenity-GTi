class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :deadline
      t.string :client
      t.decimal :price
      t.string :status

      t.timestamps
    end
  end
end
